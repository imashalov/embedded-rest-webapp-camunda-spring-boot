package info.quarrymen.quickstart.restwebapp.config;

import org.springframework.boot.context.properties.ConfigurationProperties;


@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

    private Boolean demoProperty;

    public ApplicationProperties() {
    }

    public ApplicationProperties(Boolean demoProperty) {
        this.demoProperty = demoProperty;
    }

    public Boolean getDemoProperty() {
        if (demoProperty == null) {
            return false;
        }
        return demoProperty;
    }

    public void setDemoProperty(Boolean demoProperty) {
        this.demoProperty = demoProperty;
    }
}
