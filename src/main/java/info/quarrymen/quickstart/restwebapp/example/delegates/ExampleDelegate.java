package info.quarrymen.quickstart.restwebapp.example.delegates;

import info.quarrymen.quickstart.restwebapp.config.ApplicationProperties;
import info.quarrymen.quickstart.restwebapp.example.services.ExampleService;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Component("ExampleDelegate")
public class ExampleDelegate implements JavaDelegate {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExampleDelegate.class);

    private final ApplicationProperties applicationProperties;
    private final ExampleService exampleService;

    public ExampleDelegate(ApplicationProperties applicationProperties, ExampleService exampleService) {
        this.applicationProperties = applicationProperties;
        this.exampleService = exampleService;
    }

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        LOGGER.info(LocalTime.now().format(DateTimeFormatter.ISO_LOCAL_TIME));
        Boolean demoVar = applicationProperties.getDemoProperty();
        execution.setVariable("street", "new street");
        execution.setVariable("var1", "Var1 Modified");

        exampleService.exampleMethod();
    }

}
