package info.quarrymen.quickstart.restwebapp.example.services.Impl;

import info.quarrymen.quickstart.restwebapp.example.services.ExampleService;
import org.springframework.stereotype.Service;

@Service
public class ExampleServiceImpl implements ExampleService {

    @Override
    public void exampleMethod() {
        System.out.println("This is example method");
    }
}
