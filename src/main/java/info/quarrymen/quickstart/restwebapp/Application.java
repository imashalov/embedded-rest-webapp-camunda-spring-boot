package info.quarrymen.quickstart.restwebapp;

import info.quarrymen.quickstart.restwebapp.config.ApplicationProperties;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.boot.context.embedded.EmbeddedServletContainerInitializedEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@EnableConfigurationProperties(ApplicationProperties.class)
@SpringBootApplication
public class Application {

  private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);

  private static int PORT;

  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
    LOGGER.info("You can reach the web app under: http://localhost:{}/", PORT);
  }

  @Component
  public static class ServletContainerListener implements ApplicationListener<EmbeddedServletContainerInitializedEvent> {

    @Override
    public void onApplicationEvent(EmbeddedServletContainerInitializedEvent event) {
      PORT = event.getEmbeddedServletContainer().getPort();
    }

  }
}
