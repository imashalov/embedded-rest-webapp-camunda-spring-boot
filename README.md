Embedded Camunda application with embedded tomcat in spring-boot
and added rest. There is one example bmpn process called example and
sample Delegate.

Build and and install the spring boot project.
After it is started it will start embedded tomcat on 8080:
The web app will be located on http://localhost:8080


To call rest(where 'example' is a bpmn process key):
Method: POST:
http://localhost:8080/rest/engine/default/process-definition/key/example/start

example rest call:

{
  "variables": {
    "var1": {
      "value": "test"
    }

  },
  "withVariablesInReturn": true
}